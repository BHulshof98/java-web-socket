package com.company.client.workers;

import com.company.client.Reader;
import com.company.client.helpers.ClientFileHelper;
import com.company.security.Cryptographer;

import javax.crypto.SecretKey;
import java.io.*;
import java.nio.file.Files;

public class MessageSender {
    private PrintWriter writer;
    private OutputStream outputStream;
    private FileInputStream fileInputStream;
    private Cryptographer cryptographer;
    private String sendFilePath;

    public MessageSender(OutputStream outputStream, SecretKey secretKey) {
        this.outputStream = outputStream;
        this.writer = new PrintWriter(this.outputStream);
        cryptographer = new Cryptographer(secretKey);
    }

    /**
     * Sends a message to the PrintWriter.
     * @param message to send.
     */
    public void send(String message) {
        try {
            byte[] bytes = cryptographer.encrypt(message.getBytes());
            outputStream.write(bytes, 0, bytes.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send a file over the server.
     * @param receiver to send to
     * @param path location of file
     */
    public void sendSetupRequest(String receiver, String path, Reader reader) {
        File file = new File(path);
        try {
            sendFilePath = path;
            byte[] bytes = Files.readAllBytes(file.toPath());
            String message = "SFC " + receiver + " " + bytes.length + " " + getFileName(path);
            send(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send file over socket.
     * This will be received by the user once it's only listening to incoming
     * inputstreams that listen to file bytes.
     */
    public void sendFile() {
        try {
            File file = new File(sendFilePath);
            byte[] bytes = Files.readAllBytes(file.toPath());
            bytes = cryptographer.encrypt(bytes);
            outputStream.write(bytes, 0, bytes.length);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get a filename from a path.
     * @param path the file path
     * @return filename
     */
    private String getFileName(String path) {
        String[] parts = path.split("/");
        return parts[parts.length-1];
    }
}
