package com.company.client;

import com.company.client.helpers.ClientFileHelper;
import com.company.client.workers.MessageSender;
import com.company.security.Cryptographer;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.io.InputStream;

public class Reader implements Runnable {
    private InputStream inputStream;
    private ClientFileHelper fileHelper;
    private Cryptographer cryptographer;
    private MessageSender messageSender;
    private boolean isActive = true;
    private boolean isReceivingFile = false;


    public Reader(MessageSender messageSender,
                  InputStream inputStream, SecretKey secretKey) {
        this.messageSender = messageSender;
        this.inputStream = inputStream;
        cryptographer = new Cryptographer(secretKey);
    }

    @Override
    public void run() {
        while (isActive) {
            try {
                byte[] buffer;

                buffer = new byte[inputStream.available()];

                while (inputStream.available() > 0)
                {
                    int read = inputStream.read(buffer);
                    if(read == 0)
                        break;
                }

                if(buffer.length > 0) {
                    if (!isReceivingFile) {
                        handleInput(new String(cryptographer.decrypt(buffer)));
                    } else {
                        fileHelper.setFileBytes(cryptographer.decrypt(buffer));
                        fileHelper.saveFile();
                        isReceivingFile = false;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }

    }

    /**
     * Handle the user input form the console.
     * @param input user input from console
     */
    private void handleInput(String input) {
        try {
            if(input.equals("FSO")) {
                isReceivingFile = true;
                messageSender.sendFile();
            } else if (input.equals("RFO")) {
                // TODO: remove following line after release.
                //messageSender.sendFile(fileHelper.getFilename());
            } else if (input.equals("PING")) { // If we get a PING message we send back a PONG message.
                messageSender.send("PONG");
            } else if (input.contains("FILE")) {
                setupFileAccept(input);
            } else {
                System.out.println(input);
            }
        } catch (Exception ex) {
            isActive = false;
        }
    }

    /**
     * Setup the file helper for the client that's going to receive a file.
     * @param line command
     */
    private void setupFileAccept(String line) {
        String[] args = line.split(" ");
        if(args[0].equals("FILE")) {
            fileHelper = new ClientFileHelper(args[1], Integer.valueOf(args[2]));
        }
    }
}
