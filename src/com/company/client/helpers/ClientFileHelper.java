package com.company.client.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ClientFileHelper {
    private String filename;
    private byte[] fileBytes;

    public ClientFileHelper(String filename, int size) {
        this.filename = filename;
        this.fileBytes = new byte[size];
    }

    /**
     * Get the file name of the file that will be saved.
     * @return name of the file
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Get the bytes of the file that will be saved.
     * @param fileBytes bytes of the file
     */
    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }

    /**
     * Save the file to files/[filename]
     * @throws IOException
     */
    public void saveFile() throws IOException {
        File file = new File("received/" + getFilename());
        file.getParentFile().mkdirs();
        file.createNewFile();

        try {
            OutputStream os = new FileOutputStream(file);
            os.write(fileBytes);
            os.close();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }
}
