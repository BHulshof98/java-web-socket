package com.company.client;

import com.company.client.workers.MessageSender;

import java.util.Scanner;

public class Sender implements Runnable {
    private Scanner scanner = new Scanner(System.in);
    private MessageSender messageSender;
    private Reader reader;
    private boolean isActive = true;

    public Sender(MessageSender messageSender, Reader reader) {
        this.messageSender = messageSender;
        this.reader = reader;
    }

    @Override
    public void run() {
        while(isActive) {
            String msg = scanner.nextLine(); // Get input from user.
            if(!msg.split(" ")[0].equals("FILE")) {
                messageSender.send(msg); // Send input.
                if (msg.equals("DSCN")) {
                    isActive = false;
                }
            } else {
                String receiver = msg.split(" ")[1];
                String file = msg.split(" ")[2];
                messageSender.sendSetupRequest(receiver, getFileName(file), reader);
            }
        }
    }

    /**
     * Get the filename from the path.
     * @param input file path
     * @return filename
     */
    private String getFileName(String input) {
        String[] content = input.split(" ");
        String name = "";

        for(int i = 0; i < content.length; i++) {
            name += content[i];
        }
        return name;
    }
}
