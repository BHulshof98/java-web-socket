package com.company.server;

import com.company.runnables.ConnectionThread;
import com.company.security.Cryptographer;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    private ServerSocket serverSocket;
    public Cryptographer cryptographer;
    private ArrayList<ConnectionThread> clientThreads;

    public Server(int port, SecretKey secretKey) throws IOException {
        serverSocket = new ServerSocket(port);
        clientThreads = new ArrayList<>();
        cryptographer = new Cryptographer(secretKey);
        while(true) {
            try {
                Socket socket = serverSocket.accept();
                ConnectionThread ct = new ConnectionThread(this, socket);
                clientThreads.add(ct);
                Thread thread = new Thread(ct);
                thread.start();
            } catch (Exception ex) {
                ex.printStackTrace();
                break;
            }
        }
    }

    /**
     * Broadcast a message to all connections.
     * @param from client
     * @param message to broadcast
     */
    public void broadcastMessage(String from, String message) {
        for(ConnectionThread client: clientThreads) {
            client.writeToClient("["+from.toUpperCase()+"](BROADCAST): " + message);
        }
    }

    /**
     * Sends a private message between connections.
     * @param from connection
     * @param to connection
     * @param message to send
     */
    public void sendPrivateMessage(ConnectionThread from, String to, String message) {
        for(ConnectionThread client: clientThreads) {
            if(client.getName().equals(to)) {
                client.writeToClient("["+from.getName().toUpperCase()+"](private): " + message);
                return;
            }
        }
        from.writeToClient("User could not be found!");
    }

    /**
     * Remove a connection when it has disconnected.
     * @param connectionThread client to remove.
     */
    public void connectionQuit(ConnectionThread connectionThread) {
        clientThreads.remove(connectionThread);
    }

    /**
     * Get a connection thread by its name.
     * @param name name of the ConnectionThread
     * @return ConnectionThread
     */
    public ConnectionThread getConnection(String name) {
        for(ConnectionThread thread: clientThreads) {
            if(thread.getName().equals(name)) {
                return thread;
            }
        }
        return null;
    }

    /**
     * Kill the socket connection from the client.
     * @throws IOException Exception thrown by the close function of the socket.
     */
    public void kill() throws IOException {
        serverSocket.close();
    }
}
