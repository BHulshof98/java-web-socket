package com.company.server;

import com.company.runnables.ConnectionThread;
import com.company.server.models.Group;

import java.util.ArrayList;

/**
 * This class will store data that will be accessed all across the server socket.
 * It will simulate the purpose of a database.
 * ArrayLists are very important here since they will be dynamically filled and manipulated.
 * Just as in a database.
 */
public class DataProvider {
    private ArrayList<String> userNames;
    private ArrayList<Group> groups;
    private static DataProvider ourInstance = new DataProvider();

    synchronized public static DataProvider getInstance() {
        return ourInstance;
    }

    private DataProvider() {
        userNames = new ArrayList<>();
        groups = new ArrayList<>();
    }

    /**
     * Check if the username is available.
     * @param name to check
     * @return boolean
     */
    public boolean isAvailable(String name) {
        if(userNames.contains(name)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Add a user to the list.
     * @param name of the user
     */
    synchronized public void addUser(String name) {
        this.userNames.add(name);
    }

    /**
     * Get list of users.
     * @return list of user names
     */
    public ArrayList<String> getUserNames() {
        return userNames;
    }

    /**
     * Get list of groups.
     * @return list of group names
     */
    public ArrayList<String> getGroupNames() {
        ArrayList<String> names = new ArrayList<>();
        for(Group group: groups) {
            names.add(group.getName());
        }
        return names;
    }

    /**
     * Get a group by name.
     * @param name of the group
     * @return found group
     */
    public Group getGroup(String name) {
        for(Group group : groups) {
            if(group.getName().equals(name)) {
                return group;
            }
        }
        return null;
    }

    /**
     * Create a group.
     * @param group to create
     */
    public void addGroup(Group group) {
        this.groups.add(group);
    }

    /**
     * Remove a group.
     * @param ct user that is removing the group
     * @param groupName name of the group
     */
    public void removeGroup(ConnectionThread ct, String groupName) {
        Group group = getGroup(groupName);
        if(group != null) {
            if(group.isAdmin(ct.getName())) {
                groups.remove(group);
                ct.writeToClient("Group " + groupName + " has been deleted!");
            } else {
                ct.writeToClient("Insufficient rights!");
            }
        } else {
            ct.writeToClient("Group could not be found!");
        }
    }
}
