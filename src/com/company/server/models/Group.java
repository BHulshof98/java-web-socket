package com.company.server.models;

import com.company.runnables.ConnectionThread;

import java.util.ArrayList;

public class Group {
    private ArrayList<Member> members;
    private String name;

    public Group(String name, ConnectionThread creator) {
        members = new ArrayList<>();
        this.name = name;
        Member admin = new Member(creator, "admin");
        members.add(admin);
    }

    /**
     * Join the group.
     * @param user to join
     * @return join status (false if he is already a member)
     */
    public boolean join(ConnectionThread user) {
        if(isMember(user)) {
            return false;
        }
        Member member = new Member(user, "regular");
        members.add(member);
        return true;
    }

    /**
     * Leave a group.
     * @param member to leave
     */
    public void leave(ConnectionThread member) {
        if(isMember(member)) {
            members.remove(member);
        }
    }

    /**
     * Kick a user from a group.
     * @param invoker user who invokes the kick command
     * @param member member to kick
     */
    public void kick(ConnectionThread invoker, String member) {
        if(isAdmin(invoker.getName())) {
            if (isMember(member)) {
                for (Member m : members) {
                    if (m.getConnection().getName().equals(member)) {
                        members.remove(m);
                    }
                }
            } else {
                for (Member m : members) {
                    if (m.getRole().equals("admin")) {
                        m.getConnection().writeToClient("Could not kick " + member);
                    }
                }
            }
        } else {
            invoker.writeToClient("Insufficient rights!");
        }
    }

    /**
     * Get the name of the group.
     * @return name of the group
     */
    public String getName() {
        return name;
    }

    /**
     * Get list of members with their names and roles.
     * @return list of member tags
     */
    public ArrayList<String> getMemberTags() {
        ArrayList<String> memberNames = new ArrayList<>();

        for(Member member: members) {
            memberNames.add("[" + member.getRole() + "]" + member.getConnection().getName());
        }

        return memberNames;
    }

    /**
     * Send a message to all the members.
     * @param member the sender
     * @param message message to send
     */
    public void message(ConnectionThread member, String message) {
        if(isMember(member)) {
            for(Member mem: members) {
                mem.getConnection().writeToClient("[" + member.getName() +"]("+ name +"): " + message);
            }
        }
    }

    /**
     * Check if the someone is already a member.
     * @param member user to check
     * @return boolean
     */
    private boolean isMember(ConnectionThread member) {
        return isMember(member.getName());
    }

    /**
     * Check if someone is already a member.
     * @param name name of member.
     * @return boolean
     */
    private boolean isMember(String name) {
        for (Member m: members) {
            if(m.getConnection().getName().equals(name))
                return true;
        }
        return false;
    }

    /**
     * Check if a member with given name is an admin.
     * @param name of the member
     * @return boolean
     */
    public boolean isAdmin(String name) {
        for (Member m: members) {
            if(m.getConnection().getName().equals(name)) {
                return m.getRole().equals("admin");
            }
        }
        return false;
    }
}
