package com.company.server.models;

public enum CommandTypes {
    HELO, // login
    BSCT, // broadcast
    QUIT, // quit the server
    DSCN, // disconnect
    PM, // private message
    JOIN, // join a group
    LEAV, // leave a group
    PMG, // message to a group
    LST, // get a list of data (users, groups)
    LSTG, // get a list of group members of a group
    MKG, // make a group
    RMG, // remove a group
    KICK, // kick a member from a group
    SFC, // open a file thread
    FCS, // execute the file thread
    PONG // receive pong
}
