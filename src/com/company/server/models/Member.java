package com.company.server.models;

import com.company.runnables.ConnectionThread;

public class Member {
    private ConnectionThread user;
    private String role;

    public Member(ConnectionThread user, String role) {
        this.user = user;
        this.role = role;
    }

    /**
     * Get the role of the member.
     * @return role of the member
     */
    public String getRole() {
        return role;
    }

    /**
     * Get the connection of the member.
     * @return connection of the member
     */
    public ConnectionThread getConnection() {
        return user;
    }
}
