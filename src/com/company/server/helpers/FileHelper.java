package com.company.server.helpers;

import com.company.runnables.ConnectionThread;
import com.company.security.Cryptographer;

import java.io.IOException;

public class FileHelper {
    private ConnectionThread receiver;
    private Cryptographer cryptographer;
    private String filename;
    private byte[] fileBytes;

    public FileHelper(ConnectionThread receiver, int size, String filename, Cryptographer cryptographer) {
        this.receiver = receiver;
        this.cryptographer = cryptographer;
        fileBytes = new byte[size];
        this.filename = filename + "_" + receiver.getName()+".txt";
    }

    /**
     * Get the name of the file.
     * @return name of the file
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Get bytes of the file
     * @return bytes of file
     */
    public byte[] getFileBytes() {
        return fileBytes;
    }

    /**
     * Set new file byte array.
     * This will be read from the inputstream.
     * @param fileBytes bytes of file
     */
    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }

    /**
     * Send to the receiver.
     * Will also function as a conformation.
     * @return status if the file has been received or not.
     */
    public boolean sendToReceiver() {
        try {
            byte[] bytes = cryptographer.encrypt(fileBytes);
            receiver.getOutputStream().write(bytes, 0, bytes.length);
            receiver.writeToClient(filename);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void sendOkToReceiver() {
        receiver.toggleFileReceiveStatus();
        receiver.writeToClient("RFO");
    }

    /**
     * Send file setup command to client.
     */
    public void sendFileSetupRequest() {
        receiver.writeToClient("FILE " + filename + " " + fileBytes.length );
    }

    /**
     * Get the name of the receiver.
     * @return name of the receiver
     */
    public String getReceiverName() {
        return receiver.getName();
    }
}
