package com.company;


import com.company.client.Reader;
import com.company.client.Sender;
import com.company.client.workers.MessageSender;
import com.company.runnables.ServerThread;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;

public class Main {
    public static void main(String[] args) {
        new Main().run();
    }

    private void run() {
        try {
            SecretKey secretKey = generateSecurityKey();
            Thread st = new Thread(new ServerThread(secretKey));
            st.start();
            Thread.sleep(1000);
            System.out.println("Client ready to start...");

            // Initialize the server class.
            Socket client = new Socket("127.0.0.1", 1337);

            // Initialize the message sender class to use one printwriter and avoid any unwanted performance.
            MessageSender messageSender = new MessageSender(client.getOutputStream(), secretKey);

            Reader reader = new Reader(messageSender, client.getInputStream(), secretKey);

            // Start threads for the sender and reader runnables.
            Thread senderThread = new Thread(new Sender(messageSender, reader));
            senderThread.start();

            Thread readerThread = new Thread(reader);
            readerThread.start();
        } catch (IOException | NoSuchAlgorithmException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private SecretKey generateSecurityKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(256);
        return keyGenerator.generateKey();
    }
}
