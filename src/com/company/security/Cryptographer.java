package com.company.security;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.*;

public class Cryptographer {
    private SecretKey secretKey;
    private byte[] aad;
    private SecureRandom secureRandom;
    private byte[] IV;

    public Cryptographer(SecretKey secretKey) {
        this.secretKey = secretKey;
        secureRandom = new SecureRandom();
        IV = new byte[12];
        secureRandom.nextBytes(IV);
        aad = "association".getBytes();
    }

    /**
     * Encrypt data.
     * @param data to encrypt
     * @return encrypted data
     */
    public byte[] encrypt(byte[] data) {
        try {
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
            secureRandom.nextBytes(IV);
            GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(128, IV);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);
            cipher.updateAAD(aad);
            return toByteBuffer(cipher.doFinal(data));
        } catch (InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | NoSuchPaddingException
                | NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Decrypt data.
     * @param data to decrypt
     * @return decrypted data
     */
    public byte[] decrypt(byte[] data) {
        try {
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
            // get the data from the byte buffer
            data = fromByteBuffer(data);
            // create the gcm parameter with the received IV.
            GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(128, IV);

            cipher.init(Cipher.DECRYPT_MODE, keySpec, gcmParameterSpec);
            cipher.updateAAD(aad);
            return cipher.doFinal(data);
        } catch (InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | NoSuchPaddingException
                | NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Put the encrypted data through a byte buffer.
     * This buffer will contain information about the IV array.
     * @param data encrypted data
     * @return the ByteBuffer result as byte array
     */
    private byte[] toByteBuffer(byte[] data) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4 + IV.length + data.length);
        byteBuffer.putInt(IV.length);
        byteBuffer.put(IV);
        byteBuffer.put(data);
        return byteBuffer.array();
    }

    /**
     * Gets data from a ByteBuffer and sets up data needed for decryption.
     * @param data ByteBuffer data as byte array
     * @return ByteBuffer encrypted data
     */
    private byte[] fromByteBuffer(byte[] data) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);

        int ivLength = byteBuffer.getInt();
        if(ivLength < 12 || ivLength >= 16) {
            throw new IllegalArgumentException("invalid iv length");
        }

        IV = new byte[ivLength];
        byteBuffer.get(IV);

        byte[] remaining = new byte[byteBuffer.remaining()];
        byteBuffer.get(remaining);

        return remaining;
    }
}
