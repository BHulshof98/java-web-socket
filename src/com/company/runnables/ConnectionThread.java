package com.company.runnables;

import com.company.server.Server;
import com.company.server.DataProvider;
import com.company.server.helpers.CommandHelper;
import com.company.server.helpers.FileHelper;
import com.company.server.models.Command;
import com.company.server.models.Group;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * This class will represent a connection thread for a user.
 */
public class ConnectionThread implements Runnable {
    private Server server;
    private DataInputStream inputStream;
    private OutputStream outputStream;
    private BufferedReader bufferedReader;
    private CommandHelper commandHelper;
    private Socket socket;
    private String name;
    private FileHelper fileHelper;
    private boolean isActive = true;
    private boolean isReceivingFile = false;

    public ConnectionThread(Server server, Socket socket) {
        this.socket = socket;
        this.server = server;
        commandHelper = new CommandHelper();
    }

    @Override
    public void run() {
        try {
            setStreams(new DataInputStream(socket.getInputStream()), socket.getOutputStream());
            while (isActive) {
                try {
                    byte[] buffer = null;

                    buffer = new byte[inputStream.available()];

                    while (inputStream.available() > 0)
                    {
                        int read = inputStream.read(buffer);
                        if(read == 0)
                            break;
                    }

                    if(buffer.length > 0) {
                        byte[] decrypted = server.cryptographer.decrypt(buffer);
                        if (!isReceivingFile) {
                            getInput(new String(decrypted));
                        } else {
                            fileHelper.setFileBytes(decrypted);
                            // bytes received, now we can send the file!
                            if (fileHelper.sendToReceiver()) {
                                writeToClient(fileHelper.getReceiverName()
                                        + " received " + fileHelper.getFilename());
                                fileHelper = null;
                                isReceivingFile = false;
                            }
                        }
                    }

                    buffer = null;
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            kill();
        }

        kill();
    }

    /**
     * Get the name of a user.
     * @return name of the user
     */
    public String getName() {
        return name;
    }

    /**
     * Set the user's name.
     * @param name for the user
     */
    public void setName(String name) {
        if(DataProvider.getInstance().isAvailable(name)) {
            this.name = name;
            DataProvider.getInstance().addUser(name);
            writeToClient("Hello " + name);
        } else {
            writeToClient("Username is already taken!");
        }
    }

    /**
     * Kill the connection.
     */
    private void kill() {
        try {
            server.connectionQuit(this);
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the input and output streams for a user.
     * @param dis DataInputStream
     * @param os OutputStream
     */
    private void setStreams(DataInputStream dis, OutputStream os) {
        inputStream = dis;
        outputStream = os;
        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
    }

    /**
     * Get OutputStream of the connection.
     * @return OutputStream of connection
     */
    public OutputStream getOutputStream() {
        return outputStream;
    }

    /**
     * Write a message to a client.
     * @param message to send
     */
    public void writeToClient(String message) {
        try {
            byte[] bytes = server.cryptographer.encrypt(message.getBytes());
            outputStream.write(bytes, 0, bytes.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Leave a group.
     * @param groupName name of the group
     */
    private void leaveGroup(String groupName) {
        Group group = DataProvider.getInstance().getGroup(groupName);
        if(group != null) {
            group.leave(this);
            writeToClient("Left group " + groupName);
        } else {
            writeToClient("You are currently not in group " + groupName);
        }
    }

    /**
     * Join a group.
     * @param groupName group to join
     */
    private void joinGroup(String groupName) {
        Group group = DataProvider.getInstance().getGroup(groupName);
        if(group != null) {
            group.join(this);
            writeToClient("Joined " + groupName + "!");
        } else {
            writeToClient("Group " + groupName + " could not be found!");
        }
    }

    /**
     * Message a group.
     * @param groupName group to message
     * @param message the message
     */
    private void messageGroup(String groupName, String message) {
        Group group = DataProvider.getInstance().getGroup(groupName);
        if(group != null) {
            group.message(this, message);
        } else {
            writeToClient("Group " + groupName + " could not be found!");
        }
    }

    /**
     * Request a list corresponding to the given list name.
     * @param list name of the list
     */
    private void requestList(String list) {
        if(list.equals("groups")) {
            ArrayList<String> groups = DataProvider.getInstance().getGroupNames();
            String summary = String.join(" | ", groups);
            writeToClient("GROUPS: " + summary);
        } else if (list.equals("users")) {
            ArrayList<String> users = DataProvider.getInstance().getUserNames();
            String summary = String.join(" | ", users);
            writeToClient("USERS: " + summary);
        } else {
            writeToClient("Unknown list!");
        }
    }

    /**
     * request a list of members within a group.
     * @param groupName name of the group
     */
    private void requestGroupMemberList(String groupName) {
        Group group = DataProvider.getInstance().getGroup(groupName);
        if(group != null) {
            writeToClient(String.join(" | ", group.getMemberTags()));
        } else {
            writeToClient("Group " + groupName + " could not be found!");
        }
    }

    /**
     * Create a group.
     * @param groupName name for the group
     */
    private void createGroup(String groupName) {
        Group group = new Group(groupName, this);
        if(DataProvider.getInstance().getGroup(groupName) == null) {
            DataProvider.getInstance().addGroup(group);
            writeToClient("Group created!");
        } else {
            writeToClient("Group already exists!");
        }
    }

    /**
     * Remove a group.
     * @param groupName name of the group
     */
    private void removeGroup(String groupName) {
        DataProvider.getInstance().removeGroup(this, groupName);
    }

    /**
     * Kick a member from the group.
     * @param groupName the name of the group
     * @param member to kick
     */
    private void kickFromGroup(String groupName, String member) {
        Group group = DataProvider.getInstance().getGroup(groupName);

        if(group != null) {
            group.kick(this, member);
            writeToClient("Member " + member + " has been kicked!");
        } else {
            writeToClient("Group could not be found!");
        }
    }

    /**
     * Setup a FileHelper to help with the file transfer.
     * @param contents string array containing the receiver's name and filepath.
     */
    private void setupFileTransfer(String[] contents) {
        ConnectionThread recvr = server.getConnection(contents[0]);
        String[] fileData = contents[1].split(" ");
        if(recvr != null) {
            fileHelper = new FileHelper(recvr, Integer.valueOf(fileData[0]), fileData[1], server.cryptographer);
            fileHelper.sendFileSetupRequest();
            isReceivingFile = true;
            writeToClient("FSO");
        } else {
            writeToClient("User could not be found!");
        }
    }

    /**
     * Method to toggle the isReceivingFile status.
     */
    public void toggleFileReceiveStatus() {
        if(isReceivingFile) {
            isReceivingFile = false;
        } else {
            isReceivingFile = true;
        }
    }

    /**
     * Get the input from the client.
     * @param message contents of the client's input
     */
    private void getInput(String message) {
        Command command = commandHelper.getCommand(message);
        String[] contents;
        if(!isReceivingFile) {
            if (commandHelper.isValidCommand(command)) {
                switch (command.getType()) {
                    case "HELO":
                        setName(command.getContent());
                        break;
                    case "BSCT":
                        server.broadcastMessage(this.name, command.getContent());
                        break;
                    case "DSCN":
                        isActive = false;
                        kill();
                        break;
                    case "PM":
                        contents = commandHelper.getSecondaryContents(command.getContent());
                        server.sendPrivateMessage(this, contents[0], contents[1]);
                        break;
                    case "JOIN":
                        joinGroup(command.getContent());
                        break;
                    case "LEAV":
                        leaveGroup(command.getContent());
                        break;
                    case "PMG":
                        contents = commandHelper.getSecondaryContents(command.getContent());
                        messageGroup(contents[0], contents[1]);
                        break;
                    case "LST":
                        requestList(command.getContent());
                        break;
                    case "LSTG":
                        requestGroupMemberList(command.getContent());
                        break;
                    case "MKG":
                        createGroup(command.getContent());
                        break;
                    case "RMG":
                        removeGroup(command.getContent());
                        break;
                    case "SFC":
                        contents = commandHelper.getSecondaryContents(command.getContent());
                        setupFileTransfer(contents);
                        break;
                    case "KICK":
                        contents = commandHelper.getSecondaryContents(command.getContent());
                        kickFromGroup(contents[0], contents[1]);
                        break;
                    default:
                        writeToClient("Unkown command!");
                        break;
                }
            } else {
                writeToClient("Invalid command!");
            }
        }
    }
}
