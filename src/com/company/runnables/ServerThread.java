package com.company.runnables;

import com.company.server.Server;

import javax.crypto.SecretKey;
import java.io.IOException;

public class ServerThread implements Runnable {
    private SecretKey secretKey;

    public ServerThread(SecretKey secretKey) {
        this.secretKey = secretKey;
    }

    @Override
    public void run() {
        try {
            Server server = new Server(1337, secretKey);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
